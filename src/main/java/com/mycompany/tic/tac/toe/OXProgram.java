package com.mycompany.tic.tac.toe;

import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Memos
 */
public class OXProgram {

    static char[][] table =
    {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},
    };
    static Scanner kb = new Scanner(System.in);
    static char winner = '-';
    static boolean isFinish;
    static char player = 'X';
    static int row, col;

    public static void main(String[] args) {
        showWelcome();
        do{
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        }while(!isFinish);
        showResult();
        showBye();
    }

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int i = 0; i < table.length; i++){
            System.out.print(i+1 + "");
            for (int j = 0; j < table[i].length; j++){
                System.out.print(table[i][j]);
            }
            System.out.println(" ");
        }
    }

    static void showTurn() {
        System.out.println(player + " Turn");
    }

    static void input() {
        while(true){
            System.out.println("Please input row col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if(table[row][col] == '-'){
                table[row][col] = player;
                break;
            }
            System.out.println("Error : Table at row and col is not empty !!!");
        }
    }
    static void checkCol(){
        for(int row = 0; row < 3; row++ ){
            if(table[row][col] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
    
    static void checkRow(){
        for(int col = 0; col < 3; col++ ){
            if(table[row][col] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
    
    static void checkWin() {
        checkRow();
        checkCol();
        checkDiagonal();
        checkDraw();
    }
    
    static void checkDiagonal(){
        if (table[0][2] == (player) && table[1][1] == (player) 
                && table[2][0] == (player)) {
                winner = player;
                isFinish = true;
        }
        else if (table[0][0] == (player) && table[1][1] == (player) 
                && table[2][2] == (player)) {
                winner = player;
                isFinish = true;
            }
    }
    static void checkDraw(){
        for(int row = 0; row < table.length; row++){
            for(int col = 0; col < table[row].length; col++){
                if(table[row][col] == '-'){
                    return;
                }
            }
        }
        isFinish = true;
    }

    static void switchPlayer() {
        if(player == 'X'){
            player = 'O';
        }else{
            player = 'X';
        }
    }

    static void showResult() {
        showTable();
        if(winner == 'X'){
            System.out.println("Player X is the Winner !");
        }
        else if(winner == 'O'){
            System.out.println("Player O is the Winner !");
        }
        else{
            System.out.println("Player Draw !");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ...");
    }

}
